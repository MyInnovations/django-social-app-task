from allauth.socialaccount.signals import pre_social_login  # social_account_updated
from django.dispatch import receiver


@receiver(pre_social_login)
def social_account_added(request, sociallogin, **kwargs):
    if not sociallogin.user.is_active:  # Re-activate if inactive
        sociallogin.user.is_active = True
        sociallogin.user.save()
